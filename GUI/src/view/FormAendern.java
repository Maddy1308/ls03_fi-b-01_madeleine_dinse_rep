package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.SwingConstants;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FormAendern extends JFrame {

	private JFrame self = this;
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FormAendern frame = new FormAendern();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FormAendern() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 381, 585);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		Color bgColor = contentPane.getBackground();
		
		JLabel lblNewLabel = new JLabel("Dieser Text soll ver\u00E4ndert werden.");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(0, 21, 365, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Aufgabe 1: Hintergrundfarbe \u00E4ndern");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblNewLabel_1.setBounds(10, 70, 241, 14);
		contentPane.add(lblNewLabel_1);
		
		JButton btnRed = new JButton("Rot");
		btnRed.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(Color.RED);
			}
		});
		btnRed.setBounds(10, 95, 87, 23);
		contentPane.add(btnRed);
		
		JButton btnGreen = new JButton("Gr\u00FCn");
		btnGreen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(Color.GREEN);
			}
		});
		btnGreen.setBounds(109, 95, 117, 23);
		contentPane.add(btnGreen);
		
		JButton btnBlue = new JButton("Blau");
		btnBlue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(Color.BLUE);
			}
		});
		btnBlue.setBounds(236, 95, 119, 23);
		contentPane.add(btnBlue);
		
		JButton btnYellow = new JButton("Gelb");
		btnYellow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(Color.YELLOW);
			}
		});
		btnYellow.setBounds(10, 129, 87, 23);
		contentPane.add(btnYellow);
		
		JButton btnStandardfarbe = new JButton("Standardfarbe");
		btnStandardfarbe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(bgColor);
			}
		});
		btnStandardfarbe.setBounds(109, 129, 117, 23);
		contentPane.add(btnStandardfarbe);
		
		JButton btnFarbeWhlen = new JButton("Farbe w\u00E4hlen");
		btnFarbeWhlen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Color newColor = JColorChooser.showDialog(self, "Farbauswahl", null);
				contentPane.setBackground(newColor);
			}
		});
		btnFarbeWhlen.setBounds(236, 129, 119, 23);
		contentPane.add(btnFarbeWhlen);
		
		JLabel lblNewLabel_1_1 = new JLabel("Aufgabe 2: Text formatieren");
		lblNewLabel_1_1.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblNewLabel_1_1.setBounds(10, 163, 241, 14);
		contentPane.add(lblNewLabel_1_1);
		
		JButton btnArial = new JButton("Arial");
		btnArial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setFont(new Font("Arial", Font.PLAIN, 11));
			}
		});
		btnArial.setBounds(10, 189, 78, 23);
		contentPane.add(btnArial);
		
		JButton btnComicSansMs = new JButton("Comic Sans MS");
		btnComicSansMs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setFont(new Font("Comic Sans MS", Font.PLAIN, 12));
			}
		});
		btnComicSansMs.setFont(new Font("Dialog", Font.BOLD, 12));
		btnComicSansMs.setBounds(100, 189, 126, 23);
		contentPane.add(btnComicSansMs);
		
		JButton btnCourierNew = new JButton("Courier New");
		btnCourierNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setFont(new Font("Courier New", Font.PLAIN, 12));
			}
		});
		btnCourierNew.setBounds(236, 189, 119, 23);
		contentPane.add(btnCourierNew);
		
		JTextArea txtrHierBitteText = new JTextArea();
		txtrHierBitteText.setText("Hier bitte Text eingeben");
		txtrHierBitteText.setBounds(10, 232, 345, 22);
		contentPane.add(txtrHierBitteText);
		
		JButton btnInsLabelSchreiben = new JButton("Ins Label schreiben");
		btnInsLabelSchreiben.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setText(txtrHierBitteText.getText());
			}
		});
		btnInsLabelSchreiben.setBounds(10, 264, 156, 23);
		contentPane.add(btnInsLabelSchreiben);
		
		JButton btnTextImLabel = new JButton("Text im Label l\u00F6schen");
		btnTextImLabel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setText("");
			}
		});
		btnTextImLabel.setBounds(183, 264, 172, 23);
		contentPane.add(btnTextImLabel);
		
		JLabel lblNewLabel_1_1_1 = new JLabel("Aufgabe 3: Schriftfarbe \u00E4ndern");
		lblNewLabel_1_1_1.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblNewLabel_1_1_1.setBounds(10, 298, 241, 14);
		contentPane.add(lblNewLabel_1_1_1);
		
		JButton btnRed_1 = new JButton("Rot");
		btnRed_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setForeground(Color.RED);
			}
		});
		btnRed_1.setBounds(10, 322, 100, 23);
		contentPane.add(btnRed_1);
		
		JButton btnBlue_1 = new JButton("Blau");
		btnBlue_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setForeground(Color.BLUE);
			}
		});
		btnBlue_1.setBounds(120, 322, 103, 23);
		contentPane.add(btnBlue_1);
		
		JButton btnSchwarz = new JButton("Schwarz");
		btnSchwarz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setForeground(Color.BLACK);
			}
		});
		btnSchwarz.setBounds(236, 323, 119, 23);
		contentPane.add(btnSchwarz);
		
		JLabel lblNewLabel_1_1_1_1 = new JLabel("Aufgabe 4: Schriftgr\u00F6\u00DFe ver\u00E4ndern");
		lblNewLabel_1_1_1_1.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblNewLabel_1_1_1_1.setBounds(10, 356, 241, 14);
		contentPane.add(lblNewLabel_1_1_1_1);
		
		JButton btnBig = new JButton("+");
		btnBig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int size = lblNewLabel.getFont().getSize() +1;
				lblNewLabel.setFont(new Font(getFont().getName(), getFont().getStyle(), size));
			}
		});
		btnBig.setBounds(10, 381, 156, 23);
		contentPane.add(btnBig);
		
		JButton btnSmall = new JButton("-");
		btnSmall.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int size = lblNewLabel.getFont().getSize() -1;
				lblNewLabel.setFont(new Font(getFont().getName(), getFont().getStyle(), size));
			}
		});
		btnSmall.setBounds(183, 381, 172, 23);
		contentPane.add(btnSmall);
		
		JLabel lblNewLabel_1_1_1_1_1 = new JLabel("Aufgabe 5: Textausrichtung");
		lblNewLabel_1_1_1_1_1.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblNewLabel_1_1_1_1_1.setBounds(10, 415, 241, 14);
		contentPane.add(lblNewLabel_1_1_1_1_1);
		
		JButton btnLinks = new JButton("linksb\u00FCndig");
		btnLinks.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setHorizontalAlignment(SwingConstants.LEFT);
			}
		});
		btnLinks.setBounds(10, 440, 100, 23);
		contentPane.add(btnLinks);
		
		JButton btnZentriert = new JButton("zentriert");
		btnZentriert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
			}
		});
		btnZentriert.setBounds(120, 440, 100, 23);
		contentPane.add(btnZentriert);
		
		JButton btnRechtsbndig = new JButton("rechtsb\u00FCndig");
		btnRechtsbndig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
			}
		});
		btnRechtsbndig.setBounds(236, 440, 119, 23);
		contentPane.add(btnRechtsbndig);
		
		JLabel lblNewLabel_1_1_1_1_1_1 = new JLabel("Aufgabe 6: Programm beenden");
		lblNewLabel_1_1_1_1_1_1.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblNewLabel_1_1_1_1_1_1.setBounds(10, 480, 241, 14);
		contentPane.add(lblNewLabel_1_1_1_1_1_1);
		
		JButton btnExit = new JButton("Exit");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnExit.setBounds(10, 505, 355, 41);
		contentPane.add(btnExit);
	}
}
