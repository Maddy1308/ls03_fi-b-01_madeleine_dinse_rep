package view;

import controller.AlienDefenceController;
import model.persistence.IPersistence;
import model.persistenceDummy.PersistenceDummy;
import view.menue.MainMenue;

public class StartAlienDefence {

	public static void main(String[] args) {
		
		IPersistence 		   alienDefenceModel      = new PersistenceDummy(); //Datenhaltung
		AlienDefenceController alienDefenceController = new AlienDefenceController(alienDefenceModel); //Business-Logik
		MainMenue              mainMenue              = new MainMenue(alienDefenceController); //Präsentationsschicht
		
		mainMenue.setVisible(true);
	}
}
